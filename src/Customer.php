<?php

/**
 * Class Customer
 */
class Customer
{
    /**
     * Add a coin
     *
     * @param Turnstile $turnstile
     * @return Customer
     */
    public function addCoin(Turnstile $turnstile) : Customer
    {
        $turnstile->setCoin();
        return $this;
    }

    /**
     * Trying pass
     *
     * @param Turnstile $turnstile
     * @return bool
     */
    public function pass(Turnstile $turnstile) : bool
    {
        return $turnstile->sensor();
    }
}