<?php

/**
 * Class Turnstile
 */
class Turnstile
{
    private $lock;
    private $coin;
    private $alarm;

    /**
     * Turnstile constructor.
     *
     * @param $isLock
     */
    public function __construct($isLock)
    {
        $this->lock = $isLock;
    }

    /**
     *  Turnstile is locked
     *
     * @return Turnstile
     */
    public function lock()
    {
        $this->lock = true;
        $this->removeCoin();
        return $this;
    }

    /**
     *  Turnstile is unlocked
     *
     * @return Turnstile
     */
    public function unlock()
    {
        $this->lock = false;
        return $this;
    }

    /**
     * Check the state lock
     *
     * @return bool
     */
    public function isLock() : bool
    {
        return $this->lock;
    }

    /**
     * Fires The Sensor of The Turnstile
     *
     * @return bool
     */
    public function sensor() : bool
    {
        if ($this->isLock()) {
            $this->alarmOn();
            return false;
        }

        $this->lock();
        return true;
    }

    /**
     * Set a coin to the Turnstile
     *
     * @return Turnstile
     */
    public function setCoin() : Turnstile
    {
        $this->coin = true;
        $this->unlock();
        $this->alarmOff();
        return $this;
    }

    /**
     * Remove a coin to the Turnstile
     *
     * @return Turnstile
     */
    private function removeCoin() : Turnstile
    {
        $this->coin = false;
        return $this;
    }

    /**
     * the Alarm is On
     *
     * @return Turnstile
     */
    public function alarmOn() : Turnstile
    {
        $this->alarm = true;
        return $this;
    }

    /**
     * the Alarm is Off
     *
     * @return Turnstile
     */
    public function alarmOff() : Turnstile
    {
        $this->alarm = false;
        return $this;
    }

    /**
     * Check the state alarm
     *
     * @return bool
     */
    public function isAlarm() : bool
    {
        return $this->alarm;
    }
}