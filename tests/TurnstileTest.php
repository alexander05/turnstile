<?php

use PHPUnit\Framework\TestCase;

/**
 * @covers Turnstile
 * @covers Customer
 */
final class TurnstileTest extends TestCase
{

    /**
     * @covers Turnstile::isLock
     * @covers Customer::addCoin
     */
    public function testUnlockingTurnstile()
    {
        $turnstile = new Turnstile(true);
        $this->assertEquals(true, $turnstile->isLock());

        $customer = new Customer();
        $customer->addCoin($turnstile);
        $this->assertEquals(false, $turnstile->isLock());

    }

    /**
     * @covers Turnstile::isLock
     * @covers Customer::pass
     */
    public function testLockingTurnstile()
    {
        $turnstile = new Turnstile(false);
        $this->assertEquals(false, $turnstile->isLock());

        $customer = new Customer();
        $this->assertEquals(true, $customer->pass($turnstile));
        $this->assertEquals(true, $turnstile->isLock());
    }

    /**
     * @covers Turnstile::isLock
     * @covers Turnstile::isAlarm
     * @covers Customer::pass
     */
    public function testRaisingAlarm()
    {
        $turnstile = new Turnstile(true);
        $this->assertEquals(true, $turnstile->isLock());

        $customer = new Customer();
        $this->assertEquals(false, $customer->pass($turnstile));
        $this->assertEquals(true, $turnstile->isAlarm());
        $this->assertEquals(true, $turnstile->isLock());

        $customer->addCoin($turnstile);

        $this->assertEquals(false, $turnstile->isAlarm());
        $this->assertEquals(false, $turnstile->isLock());
    }

    /**
     * @covers Turnstile::isLock
     * @covers Customer::pass
     */
    public function testGracefullyEatingMoney()
    {
        $turnstile = new Turnstile(true);
        $this->assertEquals(true, $turnstile->isLock());

        $customer = new Customer();
        $customer->addCoin($turnstile);
        $customer->addCoin($turnstile);

        $this->assertEquals(false, $turnstile->isLock());
        $this->assertEquals(true, $customer->pass($turnstile));
        $this->assertEquals(true, $turnstile->isLock());
    }
}

